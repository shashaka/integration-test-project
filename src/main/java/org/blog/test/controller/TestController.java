package org.blog.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class TestController {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping(method = RequestMethod.GET,
            value = "/test")
    public String getString() {
        HttpEntity<String> response = restTemplate.exchange("http://www.mocky.io/v2/594f7357100000ee13af3fc0", HttpMethod.GET, null, String.class);
        return response.getBody();
    }
}
